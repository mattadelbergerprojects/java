package com.artistwagon.web.serviceimpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.TypeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.artistwagon.constant.SynapseConstant;
import com.artistwagon.web.domain.Event;
import com.artistwagon.web.domain.EventPayee;
import com.artistwagon.web.domain.User;
import com.artistwagon.web.service.GroupService;
import com.artistwagon.web.service.SynapseService;
import com.artistwagon.web.service.UserService;
import com.artistwagon.web.synapse.model.BankAccount;
import com.artistwagon.web.synapse.model.BaseSynapseClass;
import com.artistwagon.web.synapse.model.SynapseBankAccount;
import com.artistwagon.web.synapse.model.SynapseUser;
import com.artistwagon.web.view.model.CreateUserViewModel;
import com.artistwagon.web.view.model.EventViewModel;
import com.artistwagon.web.view.model.MfaVerificationViewModel;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Service("SynapseService")
public class SynapseServiceImpl implements SynapseService {

	@Autowired
	UserService userService;

	@Autowired
	GroupService groupService;

	public void startUserSession(User user) {

		Map<String, String> map = new HashMap<String, String>();
		map.put("client_id", SynapseConstant.CLIENT_ID);
		map.put("client_secret", SynapseConstant.CLIENT_SECRET);
		map.put("username", user.getSynapseUsername());
		map.put("password", user.getPassword());
		RestTemplate rest = new RestTemplate();

		try {

		@SuppressWarnings("unchecked")
		Map<String, String> result = rest.postForObject(SynapseConstant.URL + "/api/v2/user/login", map, Map.class);

		user.setSynapseSessionToken(result.get("oauth_consumer_key"));

		userService.updateUser(user);

		} catch(Exception e) {

		}

	}

	public SynapseUser createUser(CreateUserViewModel user) {

		Map<String, String> map = new HashMap<String, String>();
		map.put("client_id", SynapseConstant.CLIENT_ID);
		map.put("client_secret", SynapseConstant.CLIENT_SECRET);
		map.put("force_create", "no");
		map.put("email", user.getEmail());
		map.put("phonenumber", user.getMobileNumber());
		map.put("password", user.getHashedPassword());
		map.put("fullname", user.getFullName());
		map.put("ip_address", "127.0.0.1");
		RestTemplate rest = new RestTemplate();
		
		Gson gson = new Gson();

		String result = null;
		
		try {
			result = rest.postForObject(SynapseConstant.URL + "/api/v2/user/create", map, String.class);
		} catch (HttpClientErrorException e) {
			switch (e.getStatusCode()) {
				case BAD_REQUEST:
					String text = e.getResponseBodyAsString();
					
					System.out.println(text);

					return gson.fromJson(text, SynapseUser.class);

				default:
					// TODO: add more error checking
			}
		}

		return gson.fromJson(result, SynapseUser.class);

	}

	public void getUserDetails(User user) {

		Map<String, String> map = new HashMap<String, String>();
		map.put("oauth_consumer_key", user.getSynapseSessionToken());

		RestTemplate rest = new RestTemplate();

		@SuppressWarnings("unchecked")
		HashMap<String, HashMap<String, Double>> result = rest.postForObject(SynapseConstant.URL + "/api/v2/user/show", map, HashMap.class);

		user.setBalance(result.get("user").get("balance"));

	}

	public void makePayment(EventViewModel event) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.getUserByUsername(auth.getName());

		User payee = groupService.getUserByGroupId(event.getPayees().get(0).getGroup());
		User owner = groupService.getUserByGroupId(event.getOwner());

		BankAccount payerBankAccount = this.getPrimaryBankAccount(user);

		Double agencyFee = 0.0;
		Double amount = event.gettotalAmount();

		for(EventPayee eventPayee : event.getPayees()) {

			agencyFee = agencyFee + eventPayee.getAgencyFee();
		}

		amount = amount - agencyFee + 0.25;
		agencyFee = agencyFee + 0.25;

		Map<String, String> payeeMap = new HashMap<String, String>();
		payeeMap.put("amount", amount.toString());
		payeeMap.put("seller_id", payee.getSynapseUserId().toString());
		payeeMap.put("oauth_consumer_key", user.getSynapseSessionToken());
		payeeMap.put("bank_id", payerBankAccount.getId().toString());

		Map<String, String> agencyMap = new HashMap<String, String>();
		agencyMap.put("amount", agencyFee.toString());
		agencyMap.put("seller_id", owner.getSynapseUserId().toString());
		agencyMap.put("bank_id", payerBankAccount.getId().toString());
		agencyMap.put("oauth_consumer_key", user.getSynapseSessionToken());

		RestTemplate payeeTemplate = new RestTemplate();
		RestTemplate agencyTemplate = new RestTemplate();

		@SuppressWarnings("unchecked")
		Map<String, String> payeeResult = payeeTemplate.postForObject(SynapseConstant.URL + "/api/v2/order/add", payeeMap, Map.class);

		@SuppressWarnings("unchecked")
		Map<String, String> agencyResult = agencyTemplate.postForObject(SynapseConstant.URL + "/api/v2/order/add", agencyMap, Map.class);

	}

	public BaseSynapseClass getAllBankAccounts(User user) {

		Map<String, String> map = new HashMap<String, String>();
		map.put("oauth_consumer_key", user.getSynapseSessionToken());
		RestTemplate rest = new RestTemplate();

		String result = rest.postForObject(SynapseConstant.URL + "/api/v2/bank/show", map, String.class);

		Gson gson = new Gson();

		return gson.fromJson(result, BaseSynapseClass.class);

	}

	public BankAccount getPrimaryBankAccount(User user) {

		BaseSynapseClass baseSynapseClass = this.getAllBankAccounts(user);

		for(BankAccount bankAccount : baseSynapseClass.getBanks()) {

			if(bankAccount.getPrimaryAccount().equals("Yes")) {
				return bankAccount;
			}
		}

		return null;

	}

	public void changePrimaryBankAccount(String bankId) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.getUserByUsername(auth.getName());

		Map<String, String> map = new HashMap<String, String>();
		map.put("oauth_consumer_key", user.getSynapseSessionToken());
		map.put("bank_id", bankId);

		RestTemplate rest = new RestTemplate();

		String result = rest.postForObject(SynapseConstant.URL + "/api/v2/bank/setprimary", map, String.class);

	}

	public void unlinkBankAccount(String bankId) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.getUserByUsername(auth.getName());

		Map<String, String> map = new HashMap<String, String>();
		map.put("oauth_consumer_key", user.getSynapseSessionToken());
		map.put("bank_id", bankId);

		RestTemplate rest = new RestTemplate();

		String result = rest.postForObject(SynapseConstant.URL + "/api/v2/bank/delete", map, String.class);

	}

	public SynapseBankAccount linkBankAccount(BankAccount bankAccount, User user) {

		Map<String, String> map = new HashMap<String, String>();
		map.put("oauth_consumer_key", user.getSynapseSessionToken());
		map.put("bank", bankAccount.getBank());
		map.put("username", bankAccount.getUsername());
		map.put("password", bankAccount.getPassword());

		RestTemplate rest = new RestTemplate();

		Gson gson = new Gson();

		String result = null;

		try {
			result = rest.postForObject(SynapseConstant.URL + "/api/v2/bank/login", map, String.class);
			result = result.replace("[", "").replace("]", "");
		} catch (HttpClientErrorException e) {
			switch (e.getStatusCode()) {
				case BAD_REQUEST:
					String text = e.getResponseBodyAsString();

					return gson.fromJson(text, SynapseBankAccount.class);

				default:
					// TODO: add more error checking
			}
		}
		
		return gson.fromJson(result, SynapseBankAccount.class);

	}

	public void mfaVerification(MfaVerificationViewModel mfaVerificationViewModel) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.getUserByUsername(auth.getName());

		Map<String, String> map = new HashMap<String, String>();
		map.put("oauth_consumer_key", user.getSynapseSessionToken());
		map.put("bank", mfaVerificationViewModel.getBank());
		map.put("access_token", mfaVerificationViewModel.getAccessToken());
		map.put("mfa", mfaVerificationViewModel.getCode());

		RestTemplate rest = new RestTemplate();

		String result = rest.postForObject(SynapseConstant.URL + "/api/v2/bank/mfa", map, String.class);

	}

	public void withdrawFunds(User user) {

		BankAccount bankAccount = this.getPrimaryBankAccount(user);

		this.getUserDetails(user);

		Map<String, String> map = new HashMap<String, String>();
		map.put("oauth_consumer_key", user.getSynapseSessionToken());
		map.put("amount", user.getBalance().toString());
		map.put("bank_id", bankAccount.getId().toString());

		RestTemplate rest = new RestTemplate();

		String result = rest.postForObject(SynapseConstant.URL + "/api/v2/deposit/add", map, String.class);

	}
}
