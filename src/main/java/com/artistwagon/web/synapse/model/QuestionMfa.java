package com.artistwagon.web.synapse.model;

public class QuestionMfa {

	private Response response;

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}
	
	public static class Response {
		
		private Mfa[] mfa;

		public Mfa[] getMfa() {
			return mfa;
		}

		public void setMfa(Mfa[] mfa) {
			this.mfa = mfa;
		}		
	}
	
	public static class Mfa {
		
		private String question;
		
		public String getQuestion() {
			return question;
		}

		public void setQuestion(String question) {
			this.question = question;
		}
	}
	
}
