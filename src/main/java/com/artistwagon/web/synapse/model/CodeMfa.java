package com.artistwagon.web.synapse.model;

public class CodeMfa {
	
	private Response response;

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}
	
	public static class Response {
		
		private Mfa mfa;

		public Mfa getMfa() {
			return mfa;
		}

		public void setMfa(Mfa mfa) {
			this.mfa = mfa;
		}		
	}
	
	public static class Mfa {
		
		private String message;
		
		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}
	}
	
}
