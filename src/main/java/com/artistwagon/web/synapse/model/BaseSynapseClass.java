package com.artistwagon.web.synapse.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class BaseSynapseClass {
	
	@SerializedName("is_mfa")
	private Boolean isMfa;
	
	private List<BankAccount> banks = new ArrayList<BankAccount>();
	
	
	public List<BankAccount> getBanks() {
		return banks;
	}

	public void setBanks(List<BankAccount> banks) {
		this.banks = banks;
	}

	public Boolean getIsMfa() {
		return isMfa;
	}

	public void setIsMfa(Boolean isMfa) {
		this.isMfa = isMfa;
	}
}
