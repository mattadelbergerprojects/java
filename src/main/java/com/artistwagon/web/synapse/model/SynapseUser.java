package com.artistwagon.web.synapse.model;

import com.google.gson.annotations.SerializedName;

public class SynapseUser {

	private String username;
	
	@SerializedName("oauth_consumer_key")
	private String oauthConsumerKey;
	
	@SerializedName("user_id")
	private Integer userId;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getOauthConsumerKey() {
		return oauthConsumerKey;
	}

	public void setOauthConsumerKey(String oauthConsumerKey) {
		this.oauthConsumerKey = oauthConsumerKey;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
}
