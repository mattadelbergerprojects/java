package com.artistwagon.web.synapse.model;

import com.google.gson.annotations.SerializedName;

public class BankAccount {
	
	private Integer id;
	private String nickname;
	private String username;
	private String password;
	private String bank;

	@SerializedName("bank_name")
	private String name;
	
	@SerializedName("account_number_string")
	private String accountNumber;
	
	@SerializedName("is_buyer_default")
	private String primaryAccount;
	
	
	public Integer getId() {
		return id;
	}

	public String getPrimaryAccount() {	
		
		if(primaryAccount.equals("true")) {
			return "Yes";
		} else {
			return "No";
		}
	}

	public void setPrimaryAccount(String primaryAccount) {
		this.primaryAccount = primaryAccount;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}
	
}
