package com.artistwagon.web.synapse.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class SynapseBankAccount {
	
	@SerializedName("is_mfa")
	private Boolean isMfa;
	
	@SerializedName("success")
	private Boolean isSuccessful;
	
	private List<BankAccount> banks;
	private Response response;
	
	private CodeMfa codeMfa;
	private QuestionMfa questionMfa;
	
	
	public Boolean getIsSuccessful() {
		return isSuccessful;
	}

	public void setIsSuccessful(Boolean isSuccessful) {
		this.isSuccessful = isSuccessful;
	}

	public Boolean getIsMfa() {
		return isMfa;
	}

	public void setIsMfa(Boolean isMfa) {
		this.isMfa = isMfa;
	}

	public List<BankAccount> getBanks() {
		return banks;
	}

	public void setBanks(List<BankAccount> banks) {
		this.banks = banks;
	}

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}

	public CodeMfa getCodeMfa() {
		return codeMfa;
	}

	public void setCodeMfa(CodeMfa codeMfa) {
		this.codeMfa = codeMfa;
	}

	public QuestionMfa getQuestionMfa() {
		return questionMfa;
	}

	public void setQuestionMfa(QuestionMfa questionMfa) {
		this.questionMfa = questionMfa;
	}

	public static class Response {
		
		private String type;
		
		public String getAccessToken() {
			return accessToken;
		}

		public void setAccessToken(String accessToken) {
			this.accessToken = accessToken;
		}

		@SerializedName("access_token")
		private String accessToken;


		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}
	}
	
	public static class BankAccount {
		
		private String nickname;
		
		@SerializedName("account_number_string")
		private String accountNumber;
		
		@SerializedName("bank_name")
		private String bankName;
		
		@SerializedName("is_buyer_default")
		private String primaryAccount;

		public String getPrimaryAccount() {
			return primaryAccount;
		}

		public void setPrimaryAccount(String primaryAccount) {
			this.primaryAccount = primaryAccount;
		}

		public String getAccountNumber() {
			return accountNumber;
		}

		public void setAccountNumber(String accountNumber) {
			this.accountNumber = accountNumber;
		}
		
		public String getNickname() {
			return nickname;
		}

		public void setNickname(String nickname) {
			this.nickname = nickname;
		}

		public String getBankName() {
			return bankName;
		}

		public void setBankName(String bankName) {
			this.bankName = bankName;
		}
	}
	
}
