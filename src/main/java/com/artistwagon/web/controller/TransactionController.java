package com.artistwagon.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.artistwagon.web.domain.User;
import com.artistwagon.web.service.TransactionService;
import com.artistwagon.web.service.UserService;

@Controller
public class TransactionController {
	
	@Autowired
	TransactionService transactionService;
	
	@Autowired
	UserService userService;
	
	@RequestMapping(value = {"app/withdraw"}, method = RequestMethod.GET)
	public RedirectView createEvent(
			RedirectAttributes redirectAttributes) {
		
		RedirectView redirectView = new RedirectView();
		redirectView.setContextRelative(true);
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User currentUser = userService.getUserByUsername(auth.getName());
			
		transactionService.withdrawFunds(currentUser);
		
		redirectAttributes.addFlashAttribute("success", "Your money was successfully withdrawn.");
		redirectView.setUrl("/app/events");
			
		return redirectView;		
	}
	
}
