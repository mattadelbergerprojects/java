package com.artistwagon.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.artistwagon.web.domain.User;
import com.artistwagon.web.service.SynapseService;
import com.artistwagon.web.service.UserService;
import com.artistwagon.web.synapse.model.BankAccount;

@Controller
public class BaseController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	SynapseService synapseService;
	
	public void setCurrentUser(ModelAndView model) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User currentUser = userService.getUserByUsername(auth.getName());
		
		//synapseService.getUserDetails(currentUser);
		
		model.addObject("currentUser", currentUser);
		//model.addObject("primaryBankAccount", synapseService.getPrimaryBankAccount(currentUser));

	}
	
	public void setCurrentUserWithUserObject(ModelAndView model, User user) {
		
		//synapseService.getUserDetails(user);
		
		model.addObject("currentUser", user);
		//model.addObject("primaryBankAccount", synapseService.getPrimaryBankAccount(user));

	
	}
}
