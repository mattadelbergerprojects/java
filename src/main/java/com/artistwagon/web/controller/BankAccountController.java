package com.artistwagon.web.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.artistwagon.web.domain.User;
import com.artistwagon.web.service.SynapseService;
import com.artistwagon.web.synapse.model.BankAccount;
import com.artistwagon.web.synapse.model.BaseSynapseClass;
import com.artistwagon.web.synapse.model.SynapseBankAccount;
import com.artistwagon.web.view.model.CreateEventViewModel;
import com.artistwagon.web.view.model.CreateUserViewModel;
import com.artistwagon.web.view.model.MfaVerificationViewModel;

@Controller
public class BankAccountController extends BaseController {
	
	@Autowired
	SynapseService synapseService;
	
	@RequestMapping(value = {"app/accounts"}, method = RequestMethod.GET)
	public ModelAndView viewAllBankAccounts() {
	
		ModelAndView model = new ModelAndView();
		model.setViewName("bankAccounts/all");
		
		model.addObject("title", "Bank Accounts");
		
		model.addObject("command", new BankAccount());
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User currentUser = userService.getUserByUsername(auth.getName());
		
		BaseSynapseClass baseSynapseClass = synapseService.getAllBankAccounts(currentUser);
		model.addObject("bankAccounts", baseSynapseClass.getBanks());
		
		setCurrentUserWithUserObject(model, currentUser);
		
		return model;
		
	}
	
	@RequestMapping(value = {"app/accounts/setPrimary"}, method = RequestMethod.POST)
	public @ResponseBody String changePrimaryBankAccount(
			@RequestParam("bankId") String bankId) {
		
		synapseService.changePrimaryBankAccount(bankId);
		
		return null;		
	}
	
	@RequestMapping(value = {"app/accounts/link"}, method = RequestMethod.POST)
	public RedirectView linkBankAccount(
			RedirectAttributes redirectAttributes,
			@ModelAttribute("command") BankAccount bankAccount) {
		
		RedirectView redirectView = new RedirectView();
		redirectView.setContextRelative(true);
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User currentUser = userService.getUserByUsername(auth.getName());

			bankAccount.setName(bankAccount.getBank());
			bankAccount.setUsername(bankAccount.getUsername());
			bankAccount.setPassword(bankAccount.getPassword());
			
			SynapseBankAccount synapseBankAccount = synapseService.linkBankAccount(bankAccount, currentUser);
			
			if(synapseBankAccount.getIsSuccessful() == false) {
				redirectAttributes.addFlashAttribute("error", "Bank Account has not been linked.");
				redirectView.setUrl("/app/accounts");
			} else if(synapseBankAccount.getIsMfa()) {
				redirectAttributes.addFlashAttribute("synapseBankAccount", synapseBankAccount);
				redirectAttributes.addFlashAttribute("bank", bankAccount.getBank());
				redirectView.setUrl("/app/accounts/mfa");
			} else {
				redirectAttributes.addFlashAttribute("success", "Bank Account was successfully linked.");
				redirectView.setUrl("/app/accounts");
			}
			
		return redirectView; 
		
	} 
	
	@RequestMapping(value = {"app/accounts/mfa"}, method = RequestMethod.GET)
	public ModelAndView viewMfa(
			@ModelAttribute("synapseBankAccount") SynapseBankAccount synapseBankAccount,
			@ModelAttribute("bank") String bank) {
		
		ModelAndView model = new ModelAndView();
		model.setViewName("bankAccounts/mfa");
		
		model.addObject("command", new MfaVerificationViewModel());
		
		model.addObject("accessToken", synapseBankAccount.getResponse().getAccessToken());
		model.addObject("bank", bank);
		
		if(synapseBankAccount.getCodeMfa() != null) {
			model.addObject("code", synapseBankAccount.getCodeMfa().getResponse().getMfa().getMessage());
		}
		
		if(synapseBankAccount.getQuestionMfa() != null) {
			model.addObject("questions", synapseBankAccount.getQuestionMfa().getResponse().getMfa());
		}
		
		return model;	
	} 
	
	@RequestMapping(value = {"app/accounts/mfa/link"}, method = RequestMethod.POST)
	public RedirectView linkMfa(
			RedirectAttributes redirectAttributes,
			@ModelAttribute("command") MfaVerificationViewModel mfaVerificationViewModel) {
		
		RedirectView redirectView = new RedirectView();
		redirectView.setContextRelative(true);
		
		synapseService.mfaVerification(mfaVerificationViewModel);
		redirectAttributes.addFlashAttribute("success", "Bank Account was successfully linked.");
		
		redirectView.setUrl("/app/accounts");
		
		return redirectView;
	} 
	
	@RequestMapping(value = {"app/accounts/unlinkAccount"}, method = RequestMethod.POST)
	public @ResponseBody String unlinkBankAccount(
			@RequestParam("bankId") String bankId) {
		
		synapseService.unlinkBankAccount(bankId);
		
		return null;		
	} 
	
}
