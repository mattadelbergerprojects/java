package com.artistwagon.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.artistwagon.web.domain.User;
import com.artistwagon.web.domain.UserRole;
import com.artistwagon.web.service.SynapseService;
import com.artistwagon.web.service.UserService;
import com.artistwagon.web.view.model.CreateUserViewModel;

@Controller
public class SessionController extends BaseController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	SynapseService synapseService;
	
	@RequestMapping(value = {"/app"}, method = RequestMethod.GET)
	public RedirectView defaultUrl() {
		
		RedirectView redirectView = new RedirectView();
		redirectView.setContextRelative(true);
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User currentUser = userService.getUserByUsername(auth.getName());
		
		
		if(synapseService.getPrimaryBankAccount(currentUser) != null) {
			redirectView.setUrl("/app/events");
		} else {
			redirectView.setUrl("/app/accounts");
		};
		
		
		
		return redirectView;
	
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(
		@RequestParam(value = "error", required = false) String error,
		@RequestParam(value = "logout", required = false) String logout) {
 
		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}
 
		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("login");
		
		model.addObject("title", "Login");
 
		return model;
 
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView register() {
 
		ModelAndView model = new ModelAndView();
		model.setViewName("register");
		
		model.addObject("command", new CreateUserViewModel());
 
		return model;
 
	}
	
	@RequestMapping(value = "/register/save", method = RequestMethod.POST)
	public ModelAndView createUser(@ModelAttribute("command") CreateUserViewModel createUserViewModel) {
		
		userService.createUser(createUserViewModel);
		
		ModelAndView model = new ModelAndView();
		model.setViewName("login");
				
		model.addObject("msg", "Account Successfully Created! Please login to continue.");
		 
		return model;
	}
}
