package com.artistwagon.web.view.model;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class CreateUserViewModel {
	
	private String accountType;
	private String groupName;
	private String email;
	private String fullName;
	private String mobileNumber;
	private String password;
	private String bank;
	private String bankUsername;
	private String bankPassword;
	
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	
	public String getBankUsername() {
		return bankUsername;
	}
	public void setBankUsername(String bankUsername) {
		this.bankUsername = bankUsername;
	}
	public String getBankPassword() {
		return bankPassword;
	}
	public void setBankPassword(String bankPassword) {
		this.bankPassword = bankPassword;
	}
	
	public String getHashedPassword() {
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();  
		String hashedPassword = passwordEncoder.encode(this.getPassword());
		
		return hashedPassword;
	}
	
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
}
