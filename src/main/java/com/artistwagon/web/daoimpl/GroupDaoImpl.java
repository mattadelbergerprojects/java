package com.artistwagon.web.daoimpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.artistwagon.web.dao.GroupDao;
import com.artistwagon.web.domain.Event;
import com.artistwagon.web.domain.Group;
import com.artistwagon.web.domain.User;

@Repository
public class GroupDaoImpl implements GroupDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void createGroup(Group group) {
		
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();	
		
		session.save(group);
		transaction.commit();
		
		session.close();	
	}
	
	@SuppressWarnings("unchecked")
	public List<Group> getPayers() {
		
		List<Group> groups = new ArrayList<Group>();
		
		Query query = sessionFactory.getCurrentSession()
				.createQuery("From Group Where Type='Venue' OR Type='Promoter'");
		
		groups = query.list();
		
		return groups;
	}
	
	@SuppressWarnings("unchecked")
	public List<Group> getPayees() {
		
		List<Group> groups = new ArrayList<Group>();
		
		Query query = sessionFactory.getCurrentSession()
				.createQuery("From Group Where Type='Artist'");
		
		groups = query.list();
		
		return groups;
	}
	

	public User getUserByGroupId(Group group) {
		
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("From User where group=:group")
				.setParameter("group", group);
		
		User user = (User) query.uniqueResult();
		
		session.close();
		
		return user;
		
	}

}
