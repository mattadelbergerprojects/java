package com.artistwagon.web.dao;

import java.util.List;

import com.artistwagon.web.domain.Event;
import com.artistwagon.web.domain.Group;
import com.artistwagon.web.domain.User;

public interface GroupDao {
	
	public List<Group> getPayers();
	
	public List<Group> getPayees();
	
	public User getUserByGroupId(Group group);
	
	public void createGroup(Group group);
}
