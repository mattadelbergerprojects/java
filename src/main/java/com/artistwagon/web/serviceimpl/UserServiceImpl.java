package com.artistwagon.web.serviceimpl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.artistwagon.web.dao.UserDao;
import com.artistwagon.web.domain.Group;
import com.artistwagon.web.domain.User;
import com.artistwagon.web.domain.UserRole;
import com.artistwagon.web.service.GroupService;
import com.artistwagon.web.service.SynapseService;
import com.artistwagon.web.service.UserService;
import com.artistwagon.web.synapse.model.BankAccount;
import com.artistwagon.web.synapse.model.SynapseUser;
import com.artistwagon.web.view.model.CreateUserViewModel;

@Service("userService")
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	SynapseService synapseService;
	
	@Autowired
	GroupService groupService;
	
	@Transactional(readOnly=true)
	public User getUserByUsername(String username) {
		
		return userDao.getUserByUserName(username);
		
	}
	
	@Transactional
	public void createUser(CreateUserViewModel createUserViewModel) {
		
		SynapseUser synapseUser = synapseService.createUser(createUserViewModel);
				
		User user = new User();
		user.setUsername(createUserViewModel.getEmail());
		user.setPassword(createUserViewModel.getHashedPassword());
		user.setSynapseUsername(synapseUser.getUsername());
		user.setSynapseSessionToken(synapseUser.getOauthConsumerKey());
		user.setSynapseUserId(synapseUser.getUserId());
		
		Group group = new Group();
		group.setName(createUserViewModel.getGroupName());
		group.setType(createUserViewModel.getAccountType());
		
		groupService.createGroup(group);
		
		user.setGroup(group);
		
		user.setEnabled(true);
				
		UserRole userRole = new UserRole();
		userRole.setUser(user);
		user.getUserRoles().add(userRole);
		
		BankAccount bankAccount = new BankAccount();
		bankAccount.setBank(createUserViewModel.getBank());
		bankAccount.setUsername(createUserViewModel.getBankUsername());
		bankAccount.setPassword(createUserViewModel.getBankPassword());

		userDao.createUser(user);
		
		if(bankAccount.getUsername() != "") {
			synapseService.linkBankAccount(bankAccount, user);
		}

		

		
	}
		
	@Transactional
	public void updateUser(User user) {
		
		userDao.updateUser(user);
		
	}
}
