package com.artistwagon.web.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.artistwagon.web.dao.EventDao;
import com.artistwagon.web.domain.Event;
import com.artistwagon.web.domain.EventPayee;
import com.artistwagon.web.domain.User;
import com.artistwagon.web.service.EventService;
import com.artistwagon.web.service.SynapseService;
import com.artistwagon.web.service.TransactionService;
import com.artistwagon.web.view.model.EventViewModel;

@Service("transactionService")
public class TransactionServiceImpl implements TransactionService {
	
	@Autowired
	EventService eventService;
	
	@Autowired
	SynapseService synapseService;
	
	@Transactional
	public void makePayment(Integer eventId) {
		
		EventViewModel event = eventService.getEventById(eventId);
				
		synapseService.makePayment(event);
		
		event.setStatus("Paid");
		
		eventService.updateEvent(event);
	}
	
	public void withdrawFunds(User user) {
		
		synapseService.withdrawFunds(user);
	}
	
}