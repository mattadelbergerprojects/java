package com.artistwagon.web.service;

import com.artistwagon.web.domain.User;

public interface TransactionService {
	
	public void makePayment(Integer eventId);
	
	public void withdrawFunds(User user);
 
}
