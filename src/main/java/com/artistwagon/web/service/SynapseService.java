package com.artistwagon.web.service;

import com.artistwagon.web.domain.User;
import com.artistwagon.web.synapse.model.BankAccount;
import com.artistwagon.web.synapse.model.BaseSynapseClass;
import com.artistwagon.web.synapse.model.SynapseBankAccount;
import com.artistwagon.web.synapse.model.SynapseUser;
import com.artistwagon.web.view.model.CreateUserViewModel;
import com.artistwagon.web.view.model.EventViewModel;
import com.artistwagon.web.view.model.MfaVerificationViewModel;

public interface SynapseService {

	public void startUserSession(User user);
	
	public SynapseUser createUser(CreateUserViewModel user);
	
	public void getUserDetails(User user);
	
	public void makePayment(EventViewModel event);
	
	public BaseSynapseClass getAllBankAccounts(User user);
	
	public BankAccount getPrimaryBankAccount(User user);
	
	public void withdrawFunds(User user);
	
	public void changePrimaryBankAccount(String bankId);
	
	public void unlinkBankAccount(String bankId);
	
	public SynapseBankAccount linkBankAccount(BankAccount bankAccount, User user);
	
	public void mfaVerification(MfaVerificationViewModel mfaVerificationViewModel);

}