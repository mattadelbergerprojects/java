package com.artistwagon.web.service;

import com.artistwagon.web.domain.User;
import com.artistwagon.web.view.model.CreateUserViewModel;

public interface UserService {
	
	public User getUserByUsername(String username);
	
	public void createUser(CreateUserViewModel createUserViewModel);
	
	public void updateUser(User user);
 
}
