$(document).ready(function() {
	
	$('.set-primary').click(function(e) {
		
		e.preventDefault();
		
		var bankId = $(this).closest('td').data('bank-id');
		var token = $('#token').val();
		
		request = $.ajax({
			method: "POST",
			headers: {
				'X-CSRF-Token': token
			},
	        url: "/artistwagon/app/accounts/setPrimary",
	        data: {
	        	bankId: bankId
	        },
	        success: function() {
	        	location.reload();
	        }
	    });
		
	});
	
	$('.unlink-account').click(function(e) {
		
		e.preventDefault();
		
		var bankId = $(this).closest('td').data('bank-id');
		var token = $('#token').val();
		
		request = $.ajax({
			method: "POST",
			headers: {
				'X-CSRF-Token': token
			},
	        url: "/artistwagon/app/accounts/unlinkAccount",
	        data: {
	        	bankId: bankId
	        },
	        success: function() {
	        	location.reload();
	        }
	    });
		
	});
	
});