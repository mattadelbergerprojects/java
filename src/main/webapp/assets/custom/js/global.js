$(document).ready(function() {
	
	if($('#successNotification').length>0) {
		
		var message = $('#successNotification').data('message');
		$.notify("SUCCESS: " + message, "success");
	}
	
if($('#errorNotification').length>0) {
		
		var message = $('#errorNotification').data('message');
		$.notify("ERROR: " + message, "danger");
	}

});