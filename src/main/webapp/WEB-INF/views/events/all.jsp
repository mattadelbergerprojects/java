<%@include file="/WEB-INF/views/layout/include.jspf"%>
<%@include file="/WEB-INF/views/layout/header.jspf"%>

<div class="row">
	<div class="col-md-12">	
		<c:if test="${not empty success}">
			<span data-message="${success}" id="successNotification"></span>
		</c:if>
		<div class="col-md-8 col-sm-8">
			<small>
				<ol class="breadcrumb p0 mt-sm">
		  		<li><a href="#">Home</a></li>
		  		<li class="active">Events</li>
				</ol>
			</small>	
		</div>
		<div class="col-md-4 col-sm-4">
			<c:if test="${currentUser.isAgency()}">
				<button data-toggle="modal" data-target="#createModal" class="pull-right btn btn-primary btn-lg radius-clear">Create New Event</a>
			</c:if>
		</div>
		<div class="col-md-12">
			<h1 class="mt0">Events</h1>
		</div>
		<div class="col-md-12 mt-lg">
			<table class="table table-responsive">
				<thead>
					<th>Date</th>
					<c:if test="${!currentUser.isArtist()}">
					<th>Artist</th>
					</c:if>
					<th>Venue/Promoter</th>
					<th>Total Amount</th>
					<th>Status</th>
					<th>Options</th>
				</thead>
				<tbody>
				<c:if test="${empty events}">
					<tr>
						<td class="text-center text-muted" colspan="6">You currently have no events.</td>
					</tr>
				</c:if>
				<c:forEach items="${events}" var="event">
		    	<tr>
		    		<td>${event.date}</td>
		    		<c:if test="${!currentUser.isArtist()}">
		    		<td>
		    			<c:forEach items="${event.payees}" var="payee">
		    				${payee.group.name} <br />
		    			</c:forEach>
		    		</td>
		    		</c:if>
		    		<td>${event.payer.name}</td>
		    		<td><fmt:formatNumber value="${event.totalAmount}" type="currency" /></td>
		    		<td>${event.status}</td>
		    		<td>
		    			<a href="${pageContext.request.contextPath}/app/events/${event.id}">
		    				<c:choose>
						      <c:when test="${currentUser.isPayer() && !event.isPaid()}">
						      	Pay Now
						      </c:when>
						      <c:otherwise>
						      	Open Event
						      </c:otherwise>
								</c:choose>
		    			</a>
		    		</td>
		    	</tr>
			  </c:forEach>
			</tbody>
		</table>
	</div>
	</div>
</div>

<div id="createModal" role="dialog" aria-labelledby="myModalLabelLarge" aria-hidden="false" class="modal fade">
   <div class="modal-dialog modal-lg">
      <div class="modal-content radius-clear">
      	<form:form id="createEventForm" method="POST" action="${pageContext.request.contextPath}/app/events/save">
	        <div class="modal-header">
	            <button type="button" data-dismiss="modal" aria-label="Close" class="close">
	               <span aria-hidden="true">�</span>
	            </button>
	            <h3 id="myModalLabelLarge" class="modal-title">Create Event</h3>
	         </div>
	         <div class="modal-body">
	         	<%@include file="/WEB-INF/views/events/create.jsp"%>
	         </div>
	         <div class="modal-footer">
	         	<button type=submit class="btn btn-lg btn-primary radius-clear mr-sm">Create</button>
			  		<a id="cancelEventLink" href="" data-dismiss="modal">Cancel</a>
	         </div>
         </form:form>
      </div>
   </div>
</div>
	
<%@include file="/WEB-INF/views/layout/footer.jspf"%>