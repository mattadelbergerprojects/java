<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

<div id="eventPayees" class="row">
	<div class="col-lg-6 br">
		<div class="row">
			<div class="form-group col-lg-7 ml-lg">
				<form:label path="date">Date</form:label>
				<div class='input-group date' id='datetimepicker1'>
					<form:input path="date" type="text" placeholder="Enter date" 
			 			autocomplete="off" autofocus="autofocus" data-inputmask="'mask': '99/99/9999'"
			 	 		class="form-control input-lg radius-clear" />
			 	   <span class="input-group-addon radius-clear">
	         	<span class="fa fa-calendar"></span>
	         </span>
         </div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-lg-11 ml-lg">
				<form:label path="payer">Venue/Promoter <small>(Payer)</small></form:label>
				<form:select path="payer" class="form-control input-lg radius-clear">
					<option>--- Select ---</option>
  				<form:options items="${payerDropdownList}" itemValue="id" itemLabel="name" />
				</form:select>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-lg-11 ml-lg">
				<form:label path="payees">Artist <small>(Payee)</small></form:label>
				<form:select path="payees[0].groupId" class="form-control input-lg radius-clear mb-sm">
					<option>--- Select ---</option>
	 				<form:options items="${payeesDropdownList}" itemValue="id" itemLabel="name" />
				</form:select>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-lg-6 ml-lg">
				<form:label path="payees">Amount</form:label>
				<form:input path="payees[0].amount" type="text" placeholder="Enter amount" 
		   			autocomplete="off" class="form-control input-lg radius-clear cost-input" />
			</div>
		</div>
		<div class="row">
			<div class="form-group col-lg-6 ml-lg">
				<form:label path="payees">Agency Fee</form:label>
				<form:input path="payees[0].agencyFee" type="text" placeholder="Enter fee" 
		   			autocomplete="off" class="form-control input-lg radius-clear cost-input" />
			</div>
		</div>
	  <p class="pl-lg mt-sm mb-lg"><strong>Note:</strong> Leave agency fee blank if it does not apply.</p>
			<hr />
 			<h3 class="ml-lg">Total Amount: <span id="totalAmount">$0</span></h3>
	</div>
	<div class="col-lg-6">
		<div class="mb-lg ml-lg">
			<h4 class="mt0">Information</h4>
		</div>
		<ol>
			<li class="mb-lg">Insert some information here about events.</li>
		</ol>
	</div>
</div>
