<%@include file="/WEB-INF/views/layout/include.jspf"%>
<%@include file="/WEB-INF/views/layout/header.jspf"%>


<div class="row">
	<div class="col-md-12">
		<div class="col-md-8">
			<small>
				<ol class="breadcrumb p0 mt-sm">
		  		<li><a href="#">Home</a></li>
		  		<li><a href="${pageContext.request.contextPath}/app/events">Events</a></li>
		  		<li class="active">Event Details</li>
				</ol>
			</small>
		</div>
		<div class="col-md-4 col-sm-4">
			<c:if test="${currentUser.isPayer() && !event.isPaid() && not empty primaryBankAccount}">
				<a href="${pageContext.request.contextPath}/app/events/${event.id}/makePayment" class="btn btn-primary pull-right btn-lg radius-clear">Pay Now</a>
			</c:if>
		</div>
		<div class="col-md-12">
			<h1 class="mt0">Event Details</h1>
		</div>
		<div class="col-md-12 mt-xl">
			<table class="table table-responsive">
				<tbody>
					<tr>
						<td class="col-lg-3"><strong>Status</strong></td>
						<td>${event.status}</td>
					</tr>
					<tr>
						<td class="col-lg-3"><strong>Date</strong></td>
						<td>${event.date}</td>
					</tr>
					<tr>
						<td class="col-lg-3"><strong>Venue/Promoter</strong></td>
						<td>${event.payer.name}</td>
					</tr>
					<tr>
						<td class="col-lg-3"><strong>Artist</strong></td>
						<td>${event.payees[0].group.name}</td>
					</tr>
					<c:if test="${!currentUser.isVenue()}">
						<tr>
							<td class="col-lg-3"><strong>Amount</strong></td>
							<td><fmt:formatNumber value="${event.payees[0].amount}" type="currency" /></td>
						</tr>
						<tr>
							<td class="col-lg-3"><strong>Agency Fee</strong></td>
							<td><fmt:formatNumber value="${event.payees[0].agencyFee}" type="currency" /></td>
						</tr>
					</c:if>
					<tr>
						<td class="col-lg-3"><strong>Total Amount</strong></td>
						<td><fmt:formatNumber value="${event.totalAmount}" type="currency" /></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

	
<%@include file="/WEB-INF/views/layout/footer.jspf"%>