<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

<div class="row">
	<div class="col-lg-6 br">
		<div class="row">
 	<div class="form-group col-lg-11 ml-lg">
		<form:label path="bank">Bank</form:label>
	  <form:select path="bank" class="form-control input-lg radius-clear mb-sm">
			<option>--- Select ---</option>
			<option value="Bank of America">Bank of America</option>
			<option value="Chase">Chase</option>
			<option value="Citibank">Citibank</option>
			<option value="Capital One 360">Capital One 360</option>
			<option value="Fidelity">Fidelity</option>
			<option value="US Bank">US Bank</option>
			<option value="Wells Fargo">Wells Fargo</option>
			<option value="PNC">PNC</option>
			<option value="TD Bank">TD Bank</option>
			<option value="Other">Other</option>
		</form:select>
	</div>
</div>
<div class="row">
	<div class="form-group col-lg-11 ml-lg">
		<form:label path="username">Bank Username</form:label>
	  <form:input path="username" type="text" placeholder="Enter Username" 
	   	autocomplete="off" class="form-control input-lg radius-clear" />
  </div>
</div>
  	<div class="row">
  <div class="form-group col-lg-11 ml-lg">
		<form:label path="password">Bank Password</form:label>
	  <form:input path="password" type="password" placeholder="Enter Password" 
	   	autocomplete="off" class="form-control input-lg radius-clear" />
  </div>
</div>
	</div>
	<div class="col-lg-6">
		<div class="mb-lg ml-lg">
			<h4 class="mt0">Information</h4>
		</div>
		<ol>
			<li class="mb-lg">At least one bank account must be linked to your Artist Wagon account in order to use the platform.</li>
			<li>Artist Wagon uses ACH to move funds. This usually takes <strong>1-2 business days.</strong></li>
		</ol>
	</div>
</div>

