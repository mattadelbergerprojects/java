<%@include file="/WEB-INF/views/layout/include.jspf"%>
<%@include file="/WEB-INF/views/layout/header.jspf"%>

<div class="row">
	<div class="col-md-12">
	<c:if test="${not empty success}">
			<span data-message="${success}" id="successNotification"></span>
		</c:if>
	<c:if test="${not empty error}">
			<span data-message="${error}" id="errorNotification"></span>
		</c:if>
		<div class="col-md-8 col-sm-8">
			<small>
				<ol class="breadcrumb p0 mt-sm">
		  		<li><a href="#">Home</a></li>
		  		<li class="active">Bank Accounts</li>
				</ol>
			</small>	
		</div>
		<div class="col-md-4 col-sm-4">
			<button data-toggle="modal" data-target="#linkBankAccountModal" class="pull-right btn btn-primary btn-lg radius-clear">Link Bank Account</a>
		</div>
		<div class="col-md-12">
			<h1 class="mt0">Bank Accounts</h1>
		</div>
		<div class="col-md-12 mt-lg">
			<table class="table table-responsive">
				<thead>
					<th>Nickname</th>
					<th>Bank Name</th>
					<th>Account Number</th>
					<th>Primary Account</th>
					<th>Options</th>
				</thead>
				<tbody>
				<c:if test="${empty bankAccounts}">
					<tr>
						<td class="text-center text-muted" colspan="6">
							<h3>A linked bank account must be present in order
							to begin using this platform.</h3>
						</td>
					</tr>
				</c:if>
				<c:forEach items="${bankAccounts}" var="bankAccount">
		    	<tr>
		    		<td>${bankAccount.nickname}</td>
						<td>${bankAccount.name}</td>
						<td>${bankAccount.accountNumber}</td>
						<td>${bankAccount.primaryAccount}</td>
						<td data-bank-id="${bankAccount.id}"><a class="set-primary" href="#">Make Primary</a> | <a class="unlink-account" href="#">Unlink This Account</a></td>
		    	</tr>
			  </c:forEach>
			</tbody>
		</table>
	</div>
	</div>
</div>

<div id="linkBankAccountModal" role="dialog" aria-labelledby="myModalLabelLarge" aria-hidden="false" class="modal fade">
   <div class="modal-dialog modal-lg">
      <div class="modal-content radius-clear">
      	<form:form method="POST" action="${pageContext.request.contextPath}/app/accounts/link">
	        <div class="modal-header">
	            <button type="button" data-dismiss="modal" aria-label="Close" class="close">
	               <span aria-hidden="true">�</span>
	            </button>
	            <h3 id="myModalLabelLarge" class="modal-title">Link Bank Account</h3>
	         </div>
	         <div class="modal-body">
	         		<%@include file="/WEB-INF/views/bankAccounts/link.jsp"%>
	         </div>
	         <div class="modal-footer">
	         	<button type="submit" class="btn btn-lg btn-primary radius-clear mr-sm">Link</button>
			  		<a href="" data-dismiss="modal">Cancel</a>
	         </div>
         </form:form>
      </div>
   </div>
</div>

<input id="token" type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

<%@include file="/WEB-INF/views/layout/footer.jspf"%>