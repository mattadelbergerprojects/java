<%@include file="/WEB-INF/views/layout/include.jspf"%>

<aside class="aside">
<div class="aside-inner br">
	<div class="p-lg">
		<c:if test="${!currentUser.isVenue()}">
			<p class="mb-sm">Current Balance</p>
			<h2 class="mt0"><fmt:formatNumber value="${currentUser.balance}" type="currency" /></h2>
			<c:if test="${not empty primaryBankAccount}">
				<a data-toggle="modal" data-target="#withdrawFundsModal" href="#">Withdraw Funds</a>
			</c:if>
			<hr />
		</c:if>
		<c:if test="${not empty primaryBankAccount}">
			<p class="mb-sm">Primary Account</p>
			<p class="mb0"><small>${primaryBankAccount.nickname}</small></p>
			<p><small>${primaryBankAccount.accountNumber}</small></p>
			<a href="${pageContext.request.contextPath}/app/accounts">View Bank Accounts</a>
			<hr />
		</c:if>
	</div>
</div>
</aside>

<div id="withdrawFundsModal" role="dialog" aria-labelledby="myModalLabelLarge" aria-hidden="false" class="modal fade">
   <div class="modal-dialog modal-lg">
      <div class="modal-content radius-clear">
	        <div class="modal-header">
	            <button type="button" data-dismiss="modal" aria-label="Close" class="close">
	               <span aria-hidden="true">�</span>
	            </button>
	            <h3 id="myModalLabelLarge" class="modal-title">Withdraw Funds</h3>
	         </div>
	         <div class="modal-body">
						Put Some Stuff Here
	         </div>
	         <div class="modal-footer">
	         	<a href="${pageContext.request.contextPath}/app/withdraw" class="btn btn-lg btn-primary radius-clear mr-sm">Withdraw</a>
			  		<a href="" data-dismiss="modal">Cancel</a>
	         </div>
      </div>
   </div>
</div>