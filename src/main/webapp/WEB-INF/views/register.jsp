<%@include file="/WEB-INF/views/layout/include.jspf"%>
<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <meta name="description" content="Bootstrap Admin App + jQuery">
   <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
   <title>Artist Wagon - Register</title>
   <!-- =============== VENDOR STYLES ===============-->
   <!-- FONT AWESOME-->
   <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/vendor/fontawesome/css/font-awesome.min.css">
   <!-- SIMPLE LINE ICONS-->
   <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/vendor/simple-line-icons/css/simple-line-icons.css">
   <!-- ANIMATE.CSS-->
   <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/vendor/animate.css/animate.min.css">
   <!-- WHIRL (spinners)-->
   <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/vendor/whirl/dist/whirl.css">
   <!-- =============== PAGE VENDOR STYLES ===============-->
   <!-- =============== APP STYLES ===============-->
   <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/custom/css/artistwagon.css" id="maincss">
</head>

<body id="registerPage">
	<div class="container">		
		<form:form id="registerForm" method="POST" action="${pageContext.request.contextPath}/register/save" class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3" role="form">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>	
			<div class="panel registration-panel radius-clear b animated">
				<div class="panel-heading text-center">
					<h1>Lets Get Started</h1>
					<h5>Create your basic profile below.</h5>
				</div>
				<div class="panel-body">
					<div class="form-group col-lg-12">
			      <form:label path="accountType">Account Type</form:label>
	  				<form:select path="accountType" class="form-control input-lg radius-clear mb-sm">
							<option>--- Select ---</option>
							<option value="Artist">Artist</option>
							<option value="Venue">Venue</option>
							<option value="Agency">Agency</option>
						</form:select>
		      </div>
		      <div class="form-group col-lg-12">
			      <label>Group Name</label>
			      <form:input path="groupName" type="text" placeholder="My Rock Band" class="form-control input-lg radius-clear" />
		      </div>
					<div class="form-group col-lg-12">
			      <label>Email Address</label>
			      <form:input path="email" type="text" placeholder="johndoe@email.com" class="form-control input-lg radius-clear" />
		      </div>
		      <div class="form-group col-lg-12">
			      <label>Full Name</label>
			      <form:input path="fullName" type="text" placeholder="John Doe" class="form-control input-lg radius-clear" />
		      </div>
		      <div class="form-group col-lg-12">
			      <label>Mobile Number</label>
			      <form:input path="mobileNumber" type="text" placeholder="123456789" class="form-control input-lg radius-clear"/>
		      </div>
		      <div class="form-group col-lg-12">
			      <label>Password</label>
			      <form:input path="password" type="password" placeholder="*********" class="form-control input-lg radius-clear" />
		      </div>
		      <div class="col-lg-12 mt-lg">
		      	<div class="pull-right">
		      		<button type=submit class="btn btn-lg btn-default radius-clear">Register</button>
		      	</div>
				</div>
			</div>
		</form:form>
	</div>

   <!-- =============== VENDOR SCRIPTS ===============-->
   <!-- MODERNIZR-->
   <script src="${pageContext.request.contextPath}/assets/vendor/modernizr/modernizr.js"></script>
   <!-- JQUERY-->
   <script src="${pageContext.request.contextPath}/assets/vendor/jquery/dist/jquery.js"></script>
   <!-- BOOTSTRAP-->
   <script src="${pageContext.request.contextPath}/assets/vendor/bootstrap/dist/js/bootstrap.js"></script>
   <!-- STORAGE API-->
   <script src="${pageContext.request.contextPath}/assets/vendor/jQuery-Storage-API/jquery.storageapi.js"></script>
   <!-- JQUERY EASING-->
   <script src="${pageContext.request.contextPath}/assets/vendor/jquery.easing/js/jquery.easing.js"></script>
   <!-- ANIMO-->
   <script src="${pageContext.request.contextPath}/assets/vendor/animo.js/animo.js"></script>
   <!-- SLIMSCROLL-->
   <script src="${pageContext.request.contextPath}/assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
   <!-- SCREENFULL-->
   <script src="${pageContext.request.contextPath}/assets/vendor/screenfull/dist/screenfull.min.js"></script>
   <!-- LOCALIZE-->
   <script src="${pageContext.request.contextPath}/assets/vendor/jquery-localize-i18n/dist/jquery.localize.js"></script>

   <!-- =============== PAGE VENDOR SCRIPTS ===============-->
   <!-- =============== APP SCRIPTS ===============-->
   <script src="${pageContext.request.contextPath}/assets/app/js/app.js"></script>
</body>

</html>
